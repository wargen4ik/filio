<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:57
 */

use yii\helpers\Html;

$user = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="logo">
    <div>
        <?php \yii\widgets\ActiveForm::begin(['action' => '/logout']) ?>
        <input type="submit" value="Log out" class="exit">
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
</div>

<div class="middle">
    <div class="menu">
        <div class="menu-v">
            <ul class="menu-fixed">
                <li class="menu-style"><a href="/profile">My profile</a></li>
                <li class="menu-style"><a href="/courses">My courses</a></li>
                <?= Yii::$app->user->identity->role === \app\models\User::TEACHER_ROLE ? '<li class="menu-style"><a href="/courses/create">Create course</a></li>' : ''; ?>
            </ul>
        </div>
    </div>


    <?= $content ?>

</div>
<div class="footer">
    <div style="margin-top: 50px; margin-left: 50px">Anna Havrylyshyn, 2018</div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

