<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 22:29
 */

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\IndexAsset;
use app\widgets\Alert;
use yii\helpers\Html;

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <?= Alert::widget() ?>
    <?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
