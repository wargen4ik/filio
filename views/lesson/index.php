<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:16
 */

/* @var $model \app\models\File */

\app\assets\CoursesAsset::register($this);
?>

<div class="course">
    <div class="main">
        <h1><?= $model->name ?></h1>
        <?= $model->text ?>
    </div>
</div>