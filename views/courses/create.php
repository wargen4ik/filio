<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:57
 */
\app\assets\CoursesAsset::register($this);
$user = Yii::$app->user->identity;
?>

<div class="course">
    <div class="main">
        <div class="profile-form" style="margin-top: 20px;flex: 5">
            <h1>Create course</h1>
            <?php \yii\widgets\ActiveForm::begin() ?>
            <div class="form">Course name: <input type="text" name="name" required></div>

            <div class="button">
                <input type="submit" class="edit" id="edit" value="Create">
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>

