<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:16
 */

/* @var $users \app\models\User[] */

\app\assets\CoursesAsset::register($this);

$this->registerJs(<<<JS
    $('.delete').click(function() {
        var form = $('#form');
        form.find('input[name="user_id"]').val($(this).data('id'));
        form.submit();
    });
JS
);

$this->registerCss(<<<CSS
    li > img{
        width: 25px; 
        cursor: pointer;
        border-radius: 20px;
    }
li > img:hover{
    background-color: darkred;
}
CSS
);
?>

<div class="course">
    <div class="main">
        <a style="float: right; padding-bottom: 0" class="edit" href="/courses/<?= $course->id ?>">Back</a>
        <h1>Members</h1>
        <ul style="margin-bottom: 200px">
            <?php foreach ($users as $user): ?>
                <li><img data-id="<?= $user->id ?>" class="delete"
                         src="/images/delete.png"><a href="/profile/view/<?= $user->id ?>"><?= $user->surname . ' ' . $user->first_name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php $form = \yii\widgets\ActiveForm::begin([
        'action' => '/courses/delete-member',
        'method' => 'post',
        'id' => 'form',
        'options' => [
                'style' => 'display:none'
        ]
    ]) ?>
    <input type="hidden" name="course_id" value="<?= $course->id ?>">
    <input type="hidden" name="user_id" value="">
    <?php \yii\widgets\ActiveForm::end() ?>
</div>