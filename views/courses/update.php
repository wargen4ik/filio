<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:16
 */

/* @var $course \app\models\Course */

\app\assets\CourseAsset::register($this);

$this->registerJsFile('//cdn.ckeditor.com/4.9.2/standard/ckeditor.js');

$this->registerJs(<<<JS
    $('.cb-file').change(function() {
        $(this).parent().parent().parent().find('.file-input').css('display', 'block');
        $(this).parent().parent().parent().find('.text-input').css('display', 'none');
    });
    $('.cb-text').change(function() {
        $(this).parent().parent().parent().find('.text-input').css('display', 'block');
        $(this).parent().parent().parent().find('.file-input').css('display', 'none');
    });
    function copy() {
          var copyText = document.getElementById("invite-url");
          copyText.select();
          document.execCommand("copy");
    };
    $('#url-copy').click(function() {
        copy();
    });
    
    
    $('.delete-file').click(function() {
        var me = $(this);
        $.post('/file/delete', {id: me.data('id')}, function() {
            me.parent().parent().remove();
        })
    });
    
    $('.delete-type').click(function() {
        var me = $(this);
        $.post('/type/delete', {id: me.data('id')}, function() {
            me.parent().parent().remove();
        })
    });
    
    $('.delete-lesson').click(function() {
        var me = $(this);
        $.post('/lesson/delete', {id: me.data('id')}, function() {
            me.parent().parent().remove();
        })
    });
JS
);

$this->registerCss(<<<CSS
    .theory-inputs,
    .practical-inputs,
    .file {
        text-align: center;
    }
CSS
);


$this->registerCss(<<<CSS
    .delete-file,
    .delete-lesson,
    .delete-type {
        width: 25px; 
        cursor: pointer;
        border-radius: 20px;
    }
    .delete-file:hover,
    .delete-lesson:hover,
    .delete-type:hover {
        background-color: darkred;
    }
CSS
);
?>

<div class="course">
    <div class="main">
        <h1><?= $course->name ?></h1>
        <h3 style="text-align: left">Invite url</h3>
        <input id="invite-url" type="text" value="<?= Yii::getAlias('@site/courses/join/') . $course->invitation_code ?>">
        <button class="edit" id="url-copy">Copy</button>
        <a class="edit" href="/courses/users/<?= $course->id ?>">Members</a>
        <a class="edit" style="float: right; padding-bottom: 0" href="/courses/<?= $course->id ?>">Back</a>
        <?php
        $count = 1;
        if (!$course->lessons) {
            echo '<div class="lesson"><h2>There are no lessons in this course</h2></div>';
        }
        ?>
        <?php foreach ($course->lessons as $lesson): ?>
            <div class="lesson">
                <h2>Lesson #<?= $count++ ?> <?= $lesson->name ?><img data-id="<?= $lesson->id ?>" class="delete-lesson"
                                                                     src="/images/delete.png"></h2>
                <?php foreach ($lesson->types as $type): ?>
                <div class="types">
                    <h3><?= $type->name ?><img data-id="<?= $type->id ?>" class="delete-type"
                                               src="/images/delete.png"></h3>
                    <?php foreach ($type->files as $file): ?>
                        <div class="file">
                            <div style="display: inline-block">
                            <?php if ($file->path !== 'null'): ?>
                                <?= $file->getIcon() ?> <a href="<?= $file->getFileUrl() ?>"><?= $file->name ?></a>
                                <img data-id="<?= $file->id ?>" class="delete-file"
                                     src="/images/delete.png">
                            <?php else: ?>
                                <a href="/lesson/<?= $file->id ?>"><img src="/images/text-icon.ico"></img> <?= $file->name ?></a>
                                <img data-id="<?= $file->id ?>" class="delete-file"
                                     src="/images/delete.png">
                            <?php endif ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                    <?php \yii\widgets\ActiveForm::begin(['action' => '/type/add-file?type=' . $type->id, 'options' => ['enctype' => 'multipart/form-data']]) ?>
                    <h2>Add new file or text</h2>
                    <div class="theory-inputs">
                        <div class="input-group theory">
                            <label>Enter file name or title
                                <input type="text" name="name">
                            </label>
                        </div>
                    </div>
                    <div class="theory-inputs" style="text-align: center; margin-top: 10px">
                        <div class="input-group theory">
                            <label>File</label>
                            <input type="radio" class="cb-file" required name="file-type">
                            <label>Text</label>
                            <input type="radio" class="cb-text" required name="file-type">
                        </div>
                    </div>
                    <div class="file-input" style="text-align: center; display: none">
                        <div class="input-group theory">
                            <label>Add file
                                <input type="file" name="file">
                            </label>
                        </div>
                    </div>
                    <div class="text-input" style="display: none">
                        <div class="input-group theory">
                            <textarea name="text" id="editor" class="ckeditor" rows="10" cols="80">
                            </textarea>
                        </div>
                    </div>
                    <div class="button" style="margin-top: 20px; text-align: center; margin-bottom: 20px">
                        <input type="submit" class="edit" id="edit" value="Add">
                    </div>

                    <?php \yii\widgets\ActiveForm::end() ?>
                <?php endforeach; ?>
                <?php \yii\widgets\ActiveForm::begin(['action' => '/type/create?lesson=' . $lesson->id, 'options' => ['enctype' => 'multipart/form-data']]) ?>
                <h2>Add new type</h2>
                <div class="theory-inputs">
                    <div class="input-group theory">
                        <label>Enter type name
                            <input type="text" name="name">
                        </label>
                    </div>
                </div>
                <div class="button" style="margin-top: 20px; text-align: center; margin-bottom: 20px">
                    <input type="submit" class="edit" id="edit" value="Add">
                </div>

                <?php \yii\widgets\ActiveForm::end() ?>
            </div>
        <?php endforeach; ?>
        <?php if (Yii::$app->user->identity->role === \app\models\User::TEACHER_ROLE && Yii::$app->user->id == $course->created_by): ?>
            <div class="lesson">
                <?php \yii\widgets\ActiveForm::begin(['action' => '/lesson/create?course=' . $course->id, 'options' => ['enctype' => 'multipart/form-data']]) ?>
                <h2>Add new lesson</h2>
                <div class="theory-inputs">
                    <div class="input-group theory">
                        <label for="lesson-input">Enter lesson name</label>
                        <input type="text" name="name" id="lesson-input">
                    </div>
                </div>
                <div class="button" style="margin-top: 20px; text-align: center; margin-bottom: 20px">
                    <input type="submit" class="edit" id="edit" value="Create">
                </div>

                <?php \yii\widgets\ActiveForm::end() ?>
            </div>
        <?php endif ?>
    </div>
</div>