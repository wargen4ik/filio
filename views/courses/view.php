<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:16
 */

/* @var $course \app\models\Course */
/* @var $comments \app\models\Comment[] */

\app\assets\CourseAsset::register($this);

$this->registerJsFile('//cdn.ckeditor.com/4.9.2/standard/ckeditor.js');

$this->registerJs(<<<JS

    function copy() {
          var copyText = document.getElementById("invite-url");
          copyText.select();
          document.execCommand("copy");
    };
    $('#url-copy').click(function() {
        copy();
    })
JS
);

$this->registerCss(<<<CSS
    .theory-inputs,
    .practical-inputs,
    .file {
        text-align: center;
    }
CSS
);
?>

<div class="course">
    <div class="main">
        <h1><?= $course->name ?></h1>
        <?php if (Yii::$app->user->identity->role == \app\models\User::TEACHER_ROLE && $course->created_by == Yii::$app->user->id): ?>
            <h3 style="text-align: left">Invite url</h3>
            <input id="invite-url" type="text" value="<?= Yii::getAlias('@site/courses/join/') . $course->invitation_code ?>">
            <button class="edit" id="url-copy">Copy</button>
            <a class="edit" href="/courses/users/<?= $course->id ?>">Members</a>

            <a style="float: right; padding-bottom: 0" href="/courses/update/<?= $course->id ?>" class="edit">Edit</a>
        <?php endif; ?>
        <?php
        $count = 1;
        if (!$course->lessons) {
            echo '<div class="lesson"><h2>There are no lessons in this course</h2></div>';
        }
        ?>
        <?php foreach ($course->lessons as $lesson): ?>
            <div class="lesson">
                <h2>Lesson #<?= $count++ ?> <?= $lesson->name ?></h2>
                <?php foreach ($lesson->types as $type): ?>
                    <h3><?= $type->name ?></h3>
                    <?php foreach ($type->files as $file): ?>
                        <div class="file">
                            <div style="display: inline-block">
                            <?php if ($file->path !== 'null'): ?>
                                <?= $file->getIcon() ?> <a href="<?= $file->getFileUrl() ?>"><?= $file->name ?></a>
                            <?php else: ?>
                                <a href="/lesson/<?= $file->id ?>"><img src="/images/text-icon.ico"> <?= $file->name ?></a>
                            <?php endif ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
        <?php endforeach; ?>

        <hr style="margin-top: 30px">

        <h2>Comments(<?= count($comments) ?>)</h2>
        <?php $form = \yii\widgets\ActiveForm::begin(['action' => '/courses/add-comment']) ?>
            <h4>Add comment</h4>
            <textarea name="text" required class="ckeditor">

            </textarea>
            <input type="hidden" value="<?= $course->id ?>" name="course_id">
            <input type="submit" class="edit" value="Add" style="margin-bottom: 20px">
        <?php \yii\widgets\ActiveForm::end() ?>
        <hr>
        <?php foreach ($comments as $comment): ?>
        <div class="comment">
            <div class="avatar">
                <img src="<?= $comment->createdBy->getAvatarUrl() ? : '/images/default-avatar.png' ?>" style="width: 125px; max-height: 150px">
                <p style="margin: 0"><?= $comment->createdBy->surname . ' ' . $comment->createdBy->first_name ?></p>
            </div>
            <div class="comment-body">
                <p><?= $comment->text ?></p>
            </div>
            <div class="date">
                <p><?= Yii::$app->formatter->asDatetime($comment->created_at) ?></p>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>