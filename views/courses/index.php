<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:16
 */

/* @var $courses \app\models\Course[] */

\app\assets\CoursesAsset::register($this);
?>

<div class="course">
    <div class="main">
        <h1>My courses</h1>
        <ul>
            <?php foreach ($courses as $course): ?>
                <li><a href="/courses/<?= $course->id ?>"><?= $course->name ?></a></li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>