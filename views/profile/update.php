<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:57
 */
\app\assets\ProfileAsset::register($this);
$user = Yii::$app->user->identity;
?>

<div class="course">
    <div class="main">
        <div class="image" style="flex:3">
            <img src="<?= $user->getAvatarUrl() ? : '/images/default-avatar.png' ?>">
        </div>
        <div class="profile-form" style="margin-top: 20px;flex: 5">
            <?php \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <span style="color: red"><?= $form->getFirstError('email') ?></span>
            <div class="form">Email: <input type="email" name="email" value="<?= $user->email ?>"></div>
            <div class="form">Change avatar: <input type="file" name="avatar"></div>
            <span style="color: red"><?= $form->getFirstError('old_password') ?></span>
            <div class="form">Old password: <input type="password" name="old_password"></div>
            <span style="color: red"><?= $form->getFirstError('password') ?></span>
            <div class="form">New password: <input type="password" name="password"></div>
            <div class="form">Repeat password: <input type="password" name="password_repeat""></div>
            <div class="form" style="color:#006200"><?php echo Yii::$app->session->get('success', '');
                Yii::$app->session->remove('success'); ?></div>

            <div class="button">
                <input type="submit" class="edit" id="edit" value="Edit profile">
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>

