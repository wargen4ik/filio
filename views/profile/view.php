<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:57
 */
\app\assets\ProfileAsset::register($this);

?>

<div class="course">
    <div class="main">
        <div class="image" style="flex:3">
            <img src="<?= $user->getAvatarUrl() ? : '/images/default-avatar.png' ?>">
        </div>
        <div class="profile-form" style="margin-top: 20px;flex: 5">
            <div class="form">First name: <?= $user->first_name ?></div>
            <div class="form">Second name: <?= $user->surname ?></div>
            <div class="form">Email: <?= $user->email ?></div>
            <div class="form">Registration date: <?= Yii::$app->formatter->asDate($user->created_at) ?></div>
        </div>
    </div>
</div>