<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:57
 */
\app\assets\ProfileAsset::register($this);
$user = Yii::$app->user->identity;
?>

<div class="course">
    <div class="main">
        <div class="image" style="flex:3">
            <img src="<?= $user->getAvatarUrl() ? : '/images/default-avatar.png' ?>">
        </div>
        <div class="profile-form" style="margin-top: 20px;flex: 5">
            <?php \yii\widgets\ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="form">First name: <?= $user->first_name ?></div>
            <div class="form">Second name: <?= $user->surname ?></div>
            <div class="form">Email: <?= $user->email ?></div>
            <div class="form">Registration date: <?= Yii::$app->formatter->asDate($user->created_at) ?></div>
            <div class="form" style="color:#006200"><?php echo Yii::$app->session->get('success', '');
                Yii::$app->session->remove('success'); ?></div>

            <div class="button" style="padding-bottom: 10px">
                <a class="edit" href="/profile/update" id="edit">Edit profile</a>
            </div>
            <?php \yii\widgets\ActiveForm::end() ?>
        </div>
    </div>
</div>