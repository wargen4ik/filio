<?php

/* @var $this yii\web\View */
/* @var $registerForm \app\models\RegisterForm */

/* @var $loginForm \app\models\LoginForm */

use app\assets\IndexAsset;
use yii\widgets\ActiveForm;

$this->title = 'Filio';
IndexAsset::register($this);
$modal = $login || $register ? 'block' : 'none';
$login = $login ? 'block' : 'none';
$register = $register ? 'block' : 'none';
$this->registerCss(<<<CSS
    #sign_in {
        display: {$register};
    }
    #log_in {
        display: {$login};
    }
    #reg_back {
        display: {$modal};
    }
CSS
);
?>
<div class="block">
    <div class="content">
        <div class="option">You can:
            <ul>
                <li>create your own course, on any themes and subjects</li>
                <li>add all necessary information, files, media and even video</li>
                <li>control who attend your course, add members via the link and delete them if you want, it's simple
                </li>
                <li>leave comments and important notes, everyone will see it</li>
            </ul>
        </div>
    </div>
    <div>
        <a href="#" class="button" id="reg">Sign up</a>
        <a href="#" class="button" id="log">Log in</a>
    </div>

    <div class="fon">
    </div>
</div>
<div id="reg_back"></div>
<div id="sign_in">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'action' => '/register',
//        'enableAjaxValidation' => true,
        'options' => [
            'class' => 'sign_in'
        ]
    ]) ?>
    <h1>Registration</h1>
    <div class="radio" style="text-align: center">
        <input type="radio" name="role" id="teacher" required value="2">
        <label for="teacher">Teacher</label>
        <input type="radio" name="role" id="student" required value="1">
        <label for="student">Student</label>
    </div>
    <div class="form">
        <label for="first_name">First name</label>
        <input type="text" value="<?= $registerForm->first_name ?>" name="first_name" id="first_name" required
               placeholder="Enter your first name">
    </div>
    <div class="form">
        <label for="second_name">Second name</label>
        <input type="text" value="<?= $registerForm->surname ?>" name="surname" id="second_name" required
               placeholder="Enter your second name">
    </div>
    <span style="color: red"><?= $registerForm->getFirstError('email') ?></span>
    <div class="form">
        <label for="email">Email</label>
        <input type="email" value="<?= $registerForm->email ?>" name="email" id="email" required
               placeholder="Enter email">
    </div>
    <span style="color: red"><?= $registerForm->getFirstError('password') ?></span>
    <div class="form">
        <label for="pass">Password</label>
        <input type="password" name="password" id="pass" required placeholder="Choose password">
    </div>
    <div class="form">
        <label for="confirm">Confirm password</label>
        <input type="password" name="password_repeat" id="confirm" required placeholder="Confirm your password">
    </div>
    <div style="text-align: center">
        <input type="submit" class="submit" value="Sign up">
    </div>
    <?php ActiveForm::end() ?>
</div>

<div id="log_in">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'action' => '/login',
//        'enableAjaxValidation' => true,
        'options' => [
            'class' => 'log_in'
        ]
    ]) ?>
    <h1>Autorization</h1>
    <span style="color: red"><?= $loginForm->getFirstError('email') ?></span>
    <span style="color: red"><?= $loginForm->getFirstError('password') ?></span>
    <div class="form"><label for="email">Email</label>
        <input type="email" name="email" id="email_1" placeholder="Enter email">
    </div>
    <div class="form">
        <label for="pass">Password</label>
        <input type="password" name="password" id="pass_1" placeholder="Enter your password">
    </div>
    <div style="text-align: center">
        <input type="submit" class="submit" value="Login">
    </div>
    <?php ActiveForm::end() ?>
</div>