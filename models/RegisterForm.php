<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegisterForm extends Model
{
    public $email;
    public $first_name;
    public $surname;
    public $role;
    public $password;
    public $password_repeat;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email', 'password', 'password_repeat', 'first_name', 'surname'], 'required'],
            [['role'], 'integer'],
            [['email'], 'email'],
            [['email'], 'isTaken'],
            [['password', 'password_repeat'], 'string', 'max' => 255],
            [['password', 'password_repeat'], 'string', 'min' => 6],

            [['password'], 'isEqual'],
            [['first_name', 'surname'], 'string', 'max' => 100],
        ];
    }

    public function isTaken($attribute){
        if (User::findByEmail($this->email)){
            $this->addError('email', 'Email is already taken');
        }
    }

    public function isEqual($attribute){
        if ($this->password !== $this->password_repeat){
            $this->addError('password', 'Passwords are not equal');
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function register()
    {
        if ($this->validate()) {
            $user = new User();
            $user->email = $this->email;
            $user->setPassword($this->password);
            $user->role = $this->role;
            $user->first_name = $this->first_name;
            $user->surname = $this->surname;

            $user->save(false);
            return Yii::$app->user->login($user);
        }
        return false;
    }

}
