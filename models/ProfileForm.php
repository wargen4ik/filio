<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class ProfileForm extends Model
{
    public $email;
    public $old_password;
    public $password;
    public $password_repeat;
    public $avatar;

    private $user;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
            [['email'], 'isTaken'],
            [['avatar'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg, jpeg'],
            [['password', 'password_repeat', 'old_password'], 'string', 'max' => 255],
            [['password', 'password_repeat', 'old_password'], 'string', 'min' => 6],

            [['old_password'], 'validatePassword'],
            [['password'], 'isEqual'],
        ];
    }

    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = Yii::$app->user->identity;

            if (!$user->validatePassword($this->old_password)) {
                $this->addError($attribute, 'Incorrect password.');
            }
        }
    }

    public function isTaken($attribute){
        if (($user = User::findByEmail($this->email)) && $user->id != Yii::$app->user->id){
            $this->addError('email', 'Email is already taken');
        }
    }


    public function isEqual($attribute){
        if ($this->password !== $this->password_repeat){
            $this->addError('password', 'Passwords are not equal');
        }
    }

    /**
     * @return bool whether the user is logged in successfully
     */
    public function update()
    {
        if ($this->validate()) {
            $user = Yii::$app->user->identity;
            $user->email = $this->email;
            if ($this->password) {
                $user->setPassword($this->password);
            }
            if ($this->uploadAvatar()) {
                if ($user->avatar){
                    $user->deleteAvatar();
                }
                $user->avatar = $this->avatar;
            }

            return $user->save(false);
        }
        return false;
    }

    public function uploadAvatar(){
        $this->avatar = UploadedFile::getInstanceByName('avatar');
        if (!$this->avatar) {
            return false;
        }
        $timestamp = (new \DateTime())->getTimestamp();
        $name = $timestamp . $this->avatar->baseName . '.' . $this->avatar->extension;
        $fullpath = static::getBasePath() . '/' . $name;
        $this->avatar->saveAs($fullpath);
        $this->avatar = $name;
        return true;
    }

    public static function getBasePath(){
        return FileHelper::normalizePath(Yii::getAlias('@webroot/uploads/avatars/'));
    }
}
