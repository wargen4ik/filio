<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 21.06.18
 * Time: 16:53
 */

namespace app\controllers;


use app\models\File;
use yii\web\Controller;
use yii\web\Response;

class FileController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionDelete()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $file = File::findOne(\Yii::$app->request->post('id'));
        $file->delete();
        return [];

    }
}