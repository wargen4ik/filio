<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 15.06.18
 * Time: 0:35
 */

namespace app\controllers;


use app\models\File;
use app\models\Lesson;
use yii\web\Response;

class LessonController extends BaseController
{

    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionCreate($course){
        $model = new Lesson();
        $model->course_id = $course;
        if ($model->load(\Yii::$app->request->post(), '') && $model->save()){

        }
        return $this->redirect('/courses/update/' . $course);
    }

    public function actionIndex($id){
        $model = File::findOne($id);
        if (!$model || !$model->text){
            return $this->redirect('/courses/' . $model->lessonType->lesson->course_id);
        }

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionDelete(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $lesson = Lesson::findOne(\Yii::$app->request->post('id'));
        $lesson->delete();
        return [];
    }
}