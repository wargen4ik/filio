<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 13.06.18
 * Time: 23:50
 */

namespace app\controllers;


use app\models\ProfileForm;
use app\models\User;
use yii\web\Controller;

class ProfileController extends BaseController
{
    public function actionIndex(){
        if (\Yii::$app->user->isGuest){
            return $this->redirect('/');
        }

        return $this->render('index');
    }

    public function actionUpdate(){

        $form = new ProfileForm();

        if ($form->load(\Yii::$app->request->post(), '') && $form->update()){
            \Yii::$app->session->set('success', 'Profile has been updated');
            return $this->redirect('/profile/index');
        }
        return $this->render('update', ['form' => $form]);
    }

    public function actionView($id){
        $user = User::findOne($id);

        return $this->render('view', ['user' => $user]);
    }
}