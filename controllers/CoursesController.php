<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 1:12
 */

namespace app\controllers;


use app\models\Comment;
use app\models\Course;
use app\models\CourseMember;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class CoursesController extends BaseController
{
    public function actionIndex(){
        if (\Yii::$app->user->isGuest){
            return $this->redirect('/');
        }
        if (\Yii::$app->user->identity->role === User::STUDENT_ROLE) {
            $courses = Course::find()->joinWith(['courseMembers'])->where(['user_id' => \Yii::$app->user->id])->all();
        } else {
            $courses = Course::find()->where(['created_by' => \Yii::$app->user->id])->all();
        }

        return $this->render('index', ['courses' => $courses]);
    }

    public function actionCreate(){
        $course = new Course();
        if ($course->load(\Yii::$app->request->post(), '') && $course->save()){
            return $this->redirect('/courses/' . $course->id);
        }

        return $this->render('create');
    }

    public function actionUpdate($id){
        $course = $this->findModel($id);

        if (\Yii::$app->user->id !== $course->created_by && !CourseMember::findOne(['user_id' => \Yii::$app->user->id, 'course_id' => $id])){
            return $this->redirect('/courses');
        }
        return $this->render('update', ['course' => $course]);
    }

    public function actionView($id){
        $course = $this->findModel($id);
        $comments = Comment::find()->where(['course_id' => $course->id])->orderBy('created_at DESC')->all();

        return $this->render('view', ['course' => $course, 'comments' => $comments]);
    }

    public function actionJoin($token){
        $course = Course::findOne(['invitation_code' => $token]);
        if (\Yii::$app->user->isGuest){
            return $this->redirect('/login');
        }

        if ($course){
            (new CourseMember(['user_id' => \Yii::$app->user->id, 'course_id' => $course->id]))->save();

            return $this->redirect('/courses/' . $course->id);
        }

        return $this->redirect('/courses');
    }

    public function actionUsers($id){
        $course = $this->findModel($id);
        $users = User::find()
            ->joinWith('courseMembers')
            ->where(['course_member.course_id' => $course->id])
            ->andWhere(['!=', 'user.id', \Yii::$app->user->id])
            ->all();

        return $this->render('members', ['course' => $course, 'users' => $users]);
    }

    public function actionDeleteMember(){
        $data = \Yii::$app->request->post();
        $course = $this->findModel($data['course_id']);
        $member = CourseMember::findOne(['user_id' => $data['user_id'], 'course_id' => $course->id]);
        if ($course && $member){
            $member->delete();
        }

        return $this->redirect('/courses/users/' . $course->id);
    }

    public function actionAddComment(){
        $data = \Yii::$app->request->post();
        $course = $this->findModel($data['course_id']);
        $comment = new Comment();

        $comment->load($data, '');
        $comment->save();

        return $this->redirect('/courses/' . $course->id);
    }

    protected function findModel($id){
        $model = Course::findOne($id);
        if (!$model){
            throw new NotFoundHttpException();
        }
        return $model;
    }
}