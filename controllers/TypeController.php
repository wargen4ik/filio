<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 19.06.18
 * Time: 17:52
 */

namespace app\controllers;


use app\models\File;
use app\models\Type;
use yii\web\Controller;
use yii\web\Response;

class TypeController extends Controller
{
    public function beforeAction($action) {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }
    
    public function actionCreate($lesson){
        $model = new Type();
        $model->lesson_id = $lesson;
        $model->load(\Yii::$app->request->post(), '');
        $model->save();
        return $this->redirect('/courses/update/' . $model->lesson->course->id);
    }

    public function actionAddFile($type){
        $model = new File();
        $model->type_id = $type;

        $model->load(\Yii::$app->request->post(), '');
        $model->upload();
        $model->save();
        return $this->redirect('/courses/update/' . $model->lessonType->lesson->course->id);
    }

    public function actionDelete(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $type = Type::findOne(\Yii::$app->request->post('id'));
        $type->delete();
        return [];
    }
}