<?php
/**
 * Created by PhpStorm.
 * User: theillko
 * Date: 14.06.18
 * Time: 23:30
 */

namespace app\controllers;


use yii\web\Controller;

class BaseController extends Controller
{
    public $layout = '@app/views/layouts/frontend/main';
}