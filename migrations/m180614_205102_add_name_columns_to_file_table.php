<?php

use yii\db\Migration;

/**
 * Handles adding name to table `file`.
 */
class m180614_205102_add_name_columns_to_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('file', 'name', $this->string(255)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('file', 'name');
    }
}
