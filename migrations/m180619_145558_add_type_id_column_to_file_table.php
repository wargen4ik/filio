<?php

use yii\db\Migration;

/**
 * Handles adding type_id to table `file`.
 */
class m180619_145558_add_type_id_column_to_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('file', 'type_id', $this->integer(11)->notNull());
        $this->dropColumn('file', 'type');
        $this->dropForeignKey('fk-file-lesson', 'file');
        $this->dropColumn('file', 'lesson_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('file', 'type_id');
        $this->addColumn('file', 'type', $this->smallInteger(1));
        $this->addColumn('file', 'lesson_id', $this->smallInteger(11));
    }
}
