<?php

use yii\db\Migration;

/**
 * Handles the creation of table `comment`.
 */
class m180613_180014_create_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('comment', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'text' => $this->text(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk-comment-user',
            'comment',
            'created_by',
            'user',
            'id'
        );

        $this->addForeignKey(
            'fk-comment-course',
            'comment',
            'course_id',
            'course',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-comment-course', 'comment');
        $this->dropForeignKey('fk-comment-course', 'comment');
        $this->dropTable('comment');
    }
}
