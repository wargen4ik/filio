<?php

use yii\db\Migration;

/**
 * Handles the creation of table `lesson`.
 */
class m180613_175602_create_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('lesson', [
            'id' => $this->primaryKey(),
            'number' => $this->integer(5)->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull(),
            'name' => $this->string(255)->notNull()
        ]);

        $this->addForeignKey(
            'fk-lesson-course',
            'lesson',
            'course_id',
            'course',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-lesson-course', 'lesson');
        $this->dropTable('lesson');
    }
}
