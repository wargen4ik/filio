<?php

use yii\db\Migration;

/**
 * Class m180619_135513_change_lesson_table
 */
class m180619_135513_change_lesson_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('type', 'lesson_id', $this->integer(11)->notNull());
        $this->dropColumn('type', 'course_id');
        $this->addColumn('file', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180619_135513_change_lesson_table cannot be reverted.\n";

        return false;
    }
    */
}
