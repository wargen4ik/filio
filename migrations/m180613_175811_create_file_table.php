<?php

use yii\db\Migration;

/**
 * Handles the creation of table `file`.
 */
class m180613_175811_create_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger(1)->notNull(),
            'path' => $this->string(511)->notNull(),
            'extension' => $this->string(63),
            'lesson_id' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk-file-lesson',
            'file',
            'lesson_id',
            'lesson',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-file-lesson', 'file');
        $this->dropTable('file');
    }
}
