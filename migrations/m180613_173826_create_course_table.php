<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course`.
 */
class m180613_173826_create_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('course', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'invitation_code' => $this->string(255)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk-course-user',
            'course',
            'created_by',
            'user',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-course-user', 'course');
        $this->dropTable('course');
    }
}
