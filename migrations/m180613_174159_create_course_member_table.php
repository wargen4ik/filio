<?php

use yii\db\Migration;

/**
 * Handles the creation of table `course_member`.
 */
class m180613_174159_create_course_member_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('course_member', [
            'id' => $this->primaryKey(),
            'course_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'created_at' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey(
            'fk-course_member-user',
            'course_member',
            'user_id',
            'user',
            'id'
        );

        $this->addForeignKey(
            'fk-course_member-course',
            'course_member',
            'course_id',
            'course',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-course_member-course', 'course_member');
        $this->dropForeignKey('fk-course_member-user', 'course_member');
        $this->dropTable('course_member');
    }
}
