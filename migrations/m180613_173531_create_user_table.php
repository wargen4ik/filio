<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m180613_173531_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255)->notNull(),
            'avatar' => $this->string(511),
            'password_hash' => $this->string(255)->notNull(),
            'first_name' => $this->string(100)->notNull(),
            'surname' => $this->string(100)->notNull(),
            'role' => $this->smallInteger(2)->notNull()->defaultValue(1),
            'created_at' => $this->integer(11)->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }
}
